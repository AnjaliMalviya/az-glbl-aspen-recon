def run(*args, spark, from_schema_name, to_schema_name, from_table_name, to_table_name, s3_path, curr_batch_id, prev_cycle_id, curr_cycle_id, primary_key, **kwargs):


    business_df = spark.sql("""
    SELECT
        <table columns>,
        batch_id
    FROM {from_schema_name}.{from_table_name}
    """.format(
        from_schema_name=from_schema_name,
        from_table_name=from_table_name
    ))

    from sharp_drivers.etl.scd import SCD
    scd = SCD()


    audit_type1 = scd.type1_with_audit(spark = spark,
                                       to_schema_name=to_schema_name,
                                       to_table_name=to_table_name,
                                       s3_path=s3_path,
                                       batch_df=business_df,
                                       curr_cycle_id=curr_cycle_id,
                                       prev_cycle_id=prev_cycle_id,
                                       row_identifiers=[primary_key])


    audit_type1.write.mode("overwrite").parquet("{s3_path}/{to_schema_name}/{to_table_name}/cycle_id={curr_cycle_id}".format(to_schema_name=to_schema_name,to_table_name=to_table_name,curr_cycle_id=curr_cycle_id,s3_path=s3_path))

    spark.sql("""ALTER TABLE {to_schema_name}.{to_table_name} recover partitions""".format(to_schema_name=to_schema_name,to_table_name=to_table_name))