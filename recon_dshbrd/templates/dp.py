def run(*args, spark, from_table_name, to_table_name, from_schema_name, to_schema_name, s3_path, curr_batch_id, curr_cycle_id,**kwargs):

    df = spark.sql("""
    SELECT 
      <table columns>,
      {curr_cycle_id} as cycle_id
    from {from_schema_name}.{from_table_name}
    where cycle_id = {curr_cycle_id}
    """.format(from_schema_name=from_schema_name, from_table_name=from_table_name, curr_cycle_id=curr_cycle_id, curr_batch_id=curr_batch_id))

    df.write.mode("overwrite").parquet("{s3_path}/{to_schema_name}/{to_table_name}".format(to_schema_name=to_schema_name, to_table_name=to_table_name, s3_path=s3_path))