import datetime
from typing import Text

def create_regex_translation_dict(*, aws_connection_id, regex_s3_search, override_ts_tag, **kwargs):
    translation_dict = {}
    from airflow.contrib.hooks.aws_hook import AwsHook
    s3_client = AwsHook(aws_conn_id=aws_connection_id).get_client_type('s3')

    def __get_translated_date(search_val: Text):
        data_dt = search_val
        nwdata_dt = data_dt.replace('-', '')
        actual_date = datetime.datetime.strptime('{}000000'.format(nwdata_dt), "%Y%m%d%H%M%S")
        translated_date = actual_date + datetime.timedelta(days=0)
        return nwdata_dt, translated_date.strftime("%Y%m%d%H%M%S")

    for s3_obj_summary, search_val in regex_s3_search.items():
        nwdata_dt, translated_date = __get_translated_date(search_val=search_val)
        translation_dict[s3_obj_summary] = (search_val, int(translated_date), nwdata_dt)
    print({'translation_dict': translation_dict})
    return {'translation_dict': translation_dict}


def filter_translation_exact(*, translation_dict, run_id, process, ts, exact_file_ct, **kwargs):
    from sharp.s3.file_filter_handler import get_from_and_to_ts
    filtered_file_name_list = []
    for obj_summ, (search, translation, nwdata_dt) in translation_dict.items():
        print("File_name: {}, Translation: {}".format(obj_summ.key, translation))
        filtered_file_name_list.append({'bucket_name': obj_summ.bucket_name, 'key': obj_summ.key})

    if (exact_file_ct == None or len(filtered_file_name_list) != exact_file_ct):
        raise Exception("Not Enough Files Found: {}, exact_file_ct: {}".format(
                len(filtered_file_name_list), exact_file_ct))


    print({'filtered_files': filtered_file_name_list})
    return {'filtered_files': filtered_file_name_list}