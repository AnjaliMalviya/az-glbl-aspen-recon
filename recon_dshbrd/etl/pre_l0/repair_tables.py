def run(*, spark, database, table_list, **kwargs):
    def alter_table(db_name, tbl_name):
        print("Repairing: {}.{}".format(db_name, tbl_name))
        spark.sql("ALTER TABLE {DATABASE_NAME}.{TABLE_NAME} RECOVER PARTITIONS".format(DATABASE_NAME=db_name,
                                                                                       TABLE_NAME=tbl_name))
        print("Finished repairing: {}.{}".format(db_name, tbl_name))

    for tableName in table_list:
        try:
            alter_table(database, tableName)

        except Exception as ex:
            print(ex)
            print("Failed repairing: {}.{}".format(database, tableName))